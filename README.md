<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->


<!-- PROJECT FIELDS -->

[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]


<!-- PROJECT DESC -->
<br />
<div align="center">
  
  <h3 align="center">LLM Voice Recognize</h3>

  <p align="center">
    Created by
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>
Wan »</strong></a>
    <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues/new?labels=bug&template=bug-report---.md">Report Bug</a>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>




### Built With

* [![Laravel][Laravel.com]][Laravel-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started


### Prerequisites

To run this rest-api you need to get installed docker previously (https://www.docker.com/). To hability the image docker open terminal and execute the next command.

  ```sh
  docker run -d -p 2700:2700 -v /opt/model:/opt/vosk-model-es/model alphacep/kaldi-es
  ```

### Installation


0. Clone the repo
   ```sh
   git clone --branch develop_wan --single-branch  https://gitlab.com/babyDriver/voicrecogniz.git
   ```
3. Install pip packages
   ```python
   
   ```
4. Replace the ffmpeg (check OS) path on `static/config.json`
   ```json
   "ffmpeg-path": "YOUR PATH"
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage



<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [x] Dessign Markdown
- [x] Add swagger documentation
- [ ] Control exception
- [ ] Others
    - [ ] Deploy an server installer
    - [ ] Support any audio format

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Your Name - [@your_twitter](https://twitter.com/your_username) - email@example.com

Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[issues-shield]: https://img.shields.io/gitlab/issues/all/58507751.svg?style=for-the-badge
[issues-url]: https://gitlab.com/babyDriver/voicrecogniz/issues
[license-shield]: 
https://img.shields.io/gitlab/license/58507751.svg?style=for-the-badge
[license-url]: https://gitlab.com/babyDriver/voicrecogniz/-/blob/master/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
[Laravel.com]: https://img.shields.io/badge/Python-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Swagger-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com